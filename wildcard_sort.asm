# You can implement any sorting algorithm you choose.  You can really go two ways with this: implement the simplest algorithm you can think of in as few lines as possible or take on a faster, but more complex algorithm like heapsort.
# Input arguments:
#	$a0 - starting memory address of the array to be sorted
#	$a1 - number of elements in array
# A comparison of a number of sorting algorithms you might attempt can be found here:
# https://en.wikipedia.org/wiki/Sorting_algorithm#Comparison_of_algorithms

# The algorithm implemented is selection sort

wildcard_sort:
	li $t0 0
	move $t1 $a0 
	li $t8 1
	li $t9 4

main_loop:
	move $t7 $a1
	subu $t7 $t7 $t8
	beq $t0 $t7  outoffor
	nop
	move $t2 $t0 
	addu $t2 $t2 $t8

	move $t3 $t1 
	addu $t3 $t3 $t9

	move $t4 $t0 
	lw $t7 0($t1)
	li $t4 0

whileloop:
	slt $t6 $t2 $a1 
	beq $t6 $0 outofwhile
	nop
	lw $t5 0($t3)

	slt $t6 $t5 $t7
	beq $t6 $zero skip_assignment
	nop
	move $t7 $t5 
	move $t4 $t3

skip_assignment:
	addu $t2 $t2 $t8
	addu $t3 $t3 $t9
	j whileloop
	nop 

outofwhile:
	beq $t4 $zero skipswap
	lw $t5 0($t1)
	sw $t5 0($t4)
	sw $t7 0($t1)

skipswap:
	addu $t0 $t0 $t8
	addu $t1 $t1 $t9
	j main_loop
	nop 

outoffor:
	return
	nop