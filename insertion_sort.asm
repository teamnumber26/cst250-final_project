# Implement insertion sort here
# Input arguments:
#	$a0 - starting memory address of the array to be sorted
#	$a1 - number of elements in array
# More information about insertion sort can be found here:
# http://en.wikipedia.org/wiki/Insertion_sort

insertion_sort:
	sll $a1, $a1, 2 # scaling the array length by considering size of each element
	addu $a1, $a0, $a1 # $t1 holding the word address immediately after where the array ends
	addiu $s4, $a0, -4	# $s4 holding the word address immediately before the start of the array
	addiu $s2, $a0, 4	# intializing j to point the second element
	outerforloop:
		slt $t2, $s2, $a1 # j goes upto the last element in the array
		beq $t2, $zero, exit_insertion_sort # checking if $s2 is at address after the array
		nop		 
		lw $s3, 0($s2) # $s3 = key for inserting one element at a time in the sorted array
		addiu $s1, $s2, -4 # intializing i = j-1
		innerwhileloop: 
			slt $t2, $s4, $s1 # i goes down upto the first element
			lw $t3, 0($s1) # $t3 = A[i]
			slt $t4, $s3, $t3 # key < A[i]
			and $t5, $t2, $t4 # checking i>0 and key<A[i]
			beq $t5, $zero, increment_j
			nop
			sw $t3, 4($s1) # A[i+1] = A[i]		
			addiu $s1, $s1, -4 # i = i - 1
			j innerwhileloop
			nop
		
		increment_j:
			sw $s3, 4($s1) # A[i+1] = key
			addiu $s2, $s2, 4
			j outerforloop
			nop	
	exit_insertion_sort:		
	return