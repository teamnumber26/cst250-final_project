# Implement bubble sort here
# Input arguments:
#	$a0 - starting memory address of the array to be sorted
#	$a1 - number of elements in array
# More information about bubble sort can be found here:
# http://en.wikipedia.org/wiki/Bubble_sort

bubble_sort:
	addiu $t0, $a1, -1 #initialize to length - 1 
	move $t1, $0 #i=0

for_i:
	beq $t1, $t0 exit #if i<n is false exit outer for loop
	nop
	move $t6, $a0
	move $t2, $t1 #j
	for_j:
		beq $t2, $a1 exit_for_i #if j<n is false exit inner for loop
		nop
		lw $t3, 0($t6) #moving a[i] element
		lw $t4, -4($t6) #moving a[i+1] element 
		slt $t5, $t4, $t3 #comparing a[i] and a[i+1]
		bne $t5, $0 goto_for_i #if swapping is not required
		nop
		sw $t4, 0($t6) #swapping process
		sw $t3, -4($t6) #swapping process
	goto_for_i:
		addiu $t2, $t2, 1 #j++
		addiu $t6, $t6, 4
		j for_j
		nop
	exit_for_i:
		addiu $t1, $t1, 1 #i++
		j for_i
		nop
exit:
	return
	nop